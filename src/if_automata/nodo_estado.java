/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata;

import java.util.ArrayList;
import java.util.List;
import if_automata.automatas.automata;
/**
 *
 * @author root
 */
public class nodo_estado {
    public boolean acept_st=false, err_st = false;
    public List<transicion> transiciones;
    public String init_code, cadena;
    public int tipo_edo=0, last_ind=0, cad_l;
    nodo_estado next_nodo;
    public boolean bucle, opcional, correcto;
    public int rev=0;
    public automata aut; // Si el estado requiere de un autómata anidado
    //init_code es el codigo partido, desde el indice en el que va el compilador
    //hasta el ultimo caracter del código
    public String id;
    
    public nodo_estado get_next_nodo(){
        return this.next_nodo;
    }
    
    public void add_transicion(transicion t){
        if(transiciones==null){
            transiciones = new ArrayList<>();
        }
        
        transiciones.add(t);
    }
    
    public void add_transicion(String c, nodo_estado nodo){
        if(transiciones==null)
            transiciones = new ArrayList<>();
        
        transicion t = new transicion();
        t.add_cadena(c);
        t.add_nodo(nodo);
        transiciones.add(t);
    }
    
    public void add_transicion(nodo_estado nodo, automata auto){
        if(transiciones==null)
            transiciones = new ArrayList<>();
        
        transicion t = new transicion();
        t.add_cadena("");
        t.add_nodo(nodo);
        t.tipo = 1;
        t.auto = auto;
        transiciones.add(t);
    }
    
    
    public nodo_estado(){
    }
    
    public nodo_estado(String cad, String i_c){
        this.cadena = cad;
        this.init_code = i_c;
    }
    
    
//    public boolean evaluar(){
//        if(acept_st){
//            if(init_code.length() > 0){
//                if(transiciones != null){
//                    return evaluar_tr();
//                }else{
//                    return false;
//                }
//            }else{
//                return true;
//            }
//        }else{
//            
//        }
//        return false;
//    }
    
    
    public boolean evaluar(){
        
//        String cad_eval = init_code.substring(
//                0,
//                cadena.length());
//        return cad_eval.equals(cadena);

//        if(this.tipo_edo == 1){
//            if(this.aut == null)
//                return false;
//            this.aut.set_fuente(this.init_code);
//            return this.aut.evaluar();
//        }else{
//            
//        }
        rev++;
        
        if(transiciones != null){ //EEMPIEZA DE AQUIII
                if(this.acept_st && this.init_code.length()==0){
                    /*
                    Si el nodo es de aceptación y ya no hay más codigo, entonces
                    el codigo esta correcto
                    */
                    rev++;
                    return true;
                }else{
                    for(transicion t : transiciones){
                        t.init_code = this.init_code;
                        /*
                        ******************
                        Falta evaluar cuando es un AFND, en el cual existen varias
                        transiciones y evaluar cual de todas si termina bien,
                        o sea, evaluar hasta donde termina el automata.
                        ******************
                        */
    //                    return t.evaluar(); //retornar el resultado de la evaluación
                        if(t.evaluar()){
                            //De aqui a mandar a evaluar todo de manera anidada
                            this.next_nodo = t.next_nodo;
                            this.last_ind = t.last_index;
//                            rev++;
                            return true;
                        }
                    }
                }
            }else{
                if(this.init_code.length() > 0 && this.tipo_edo==0 ){
                    /*Si el nodo ya no tiene transiciones (osea que es nodo terminal
                    y ya no debe de sobrar código) y sobra código, entonces se va al
                    estado de error. *** no se si todos los nodos terminales, ya no
                    deben de aceptar más codigo, pero por el momento queda asi ***
                    */
                    show_message("Sobra codigo");
                    if(this.bucle && this.rev > 0){
                        this.bucle = false;
                        this.last_ind = 0;
//                        rev++;
                        return true;
                    }
                    return false;
                } else if(this.acept_st){
                    /*
                    Si llega a este punto, quiere decir que es nodo terminal
                    */
//                    rev++;
                    return true;
                }
            }
        
        if(this.bucle && this.rev>0){
            show_message("El nodo contiene error. Pero es un bucle y tiene mas de una revision");
            this.bucle = false;
            this.last_ind = 0;
//            rev++;
            return true;
        }
        show_message("Error");
        return false;
    }
    
    
    private void show_message(String msj){
        System.out.println(String.format("%s -  Nodo: %s", msj, this.id ));
    }
    
//    public boolean evaluar_tr(){
//        for(transicion t : transiciones){
//            for(String c : t.cadenas){
//                if(init_code.length() >= c.length()){
////                    String cad_eval = init_code.substring(0, c.length());
//                    if(init_code.substring(0, c.length()).equals(c)){
//                        t.nodo.init_code = init_code.substring(c.length(), init_code.length());
//                        if(t.nodo.evaluar()){
//                            return true;
//                        }
//                    }
//                }else{
//                    //no esta correcto, no estan todos los caracteres para evaluar
//                    return false;
//                }
//            }
//        }
//        return false;
//    }
    
}
