/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata;

import if_automata.automatas.automata;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class transicion {
    
    List<String> cadenas;
    List<nodo_estado> nodos;
    nodo_estado next_nodo;
    String init_code;
    public int last_index, tipo;
    automata auto; //Aqui pueden ser muchos automatas
    
    
    public void add_cadena(String cad){
        if(cadenas==null)
            cadenas = new ArrayList<String>();
        cadenas.add(cad);
    }
    
    public void set_code(String c){
        this.init_code = c;
    }
    
    public void add_nodo(nodo_estado nodo){
        if(nodos==null)
            nodos = new ArrayList<>();
        
        nodos.add(nodo);
    }
    
    public boolean evaluar(){
        int ind=0;
        String cad_eval;
        if(this.tipo == 1){
            this.auto.set_fuente(this.init_code);
            if(this.auto.evaluar()){
                this.next_nodo = nodos.get(0);
                this.last_index = auto.ind;
                return true;
            }else{
                
                System.out.println("El automata "+this.auto.ID+" no es valido");
                return false;
            }
        }
        
        for(String c : cadenas){
            if(init_code.length() >= c.length()){
                cad_eval = init_code.substring(0, c.length());
                System.out.println("Evaluando "+cad_eval+" con "+c);
                if(cad_eval.equals(c)){
                    next_nodo = nodos.get(ind);/*
                Por el momento, como es AFD, entonces solo hay un next nodo.
                Cuando sea AFND, entonces habra una lista de next_nodo, porque
                el nodo puede tener varias opciones
                */
                    this.last_index = c.length();
                    return true;
                }
            }else{
                /*
                Si el largo del código es menor que el de la palabra esperada,
                entonces se va al estado de error.
                */
                System.out.println("Codigo insuficiente");
                return false;
            }
            ind++;
        }
        return false; //Si retorna false, quiere decir que recibio otro caracter
        // no esperado. Lo que indica que se llega al estado de error
    }
    
    
    public boolean check_code_length(int length){
//        if(init_code.length() >= length)
        return false;
    }
    
}
