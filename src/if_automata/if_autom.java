/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class if_autom {
    
    String fuente;
    String[] reservadas= {"if", "else", "and", "or"};
    int ind=0, cad_l=0;
    nodo_estado nodo_act;
    List<nodo_estado> nodos;
    
    public if_autom(String cod){
        this.fuente = cod;
        this.fuente.replace(" ", "");
        this.fuente.replace("\n", "");
    }
    
    
    public void build_automata(){
        nodos = new ArrayList<>();
        nodos.add(build_nodo(
                        false, 
                        false,
                        0
                )); //a
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // b
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // c
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // d
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // e
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // f
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // g
        nodos.add(build_nodo(
                        true,
                        false,
                        0
                )); // h
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // i
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // j
        nodos.add(build_nodo(
                        false,
                        false,
                        0
                )); // k
        nodos.add(build_nodo(
                        true,
                        false,
                        0
                )); // l
        nodos.add(build_nodo(
                        false,
                        true,
                        0
                )); // z
        
        add_transicion(0, "if", 2);
    }
    
    public void add_transicion(int ori, String cadena, int dest){
        if(nodos!=null){
            nodos.get(ori).add_transicion(cadena, nodos.get(dest));
        }
    }
    
    public nodo_estado build_nodo(boolean accept, boolean error, int tipo){
        nodo_estado n = new nodo_estado();
        n.acept_st = accept;
        n.err_st = error;
        n.tipo_edo = tipo;
        return n;
    }
    
    public boolean evaluar(){
        
        return false;
    }
    
}
