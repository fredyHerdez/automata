/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

import if_automata.nodo_estado;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fredy
 */
public class automata {

    String fuente;
    public int ind = 0;
    nodo_estado nodo_act;
    List<nodo_estado> nodos;
    public String ID;

    public void build() {

    }

    public void set_fuente(String fte) {
        this.fuente = fte;
    }

    public automata(String cod) {
        this.fuente = cod;
        nodos = new ArrayList<>();
        build();
    }

    public automata() {
        nodos = new ArrayList<>();
        build();
    }

    public void add_transicion(int ori, String cadena, int dest) {
        if (nodos != null) {
            nodos.get(ori).add_transicion(cadena, nodos.get(dest));
        }
    }

    public void add_transicion(int ori, int dest, automata auto) {
        if (nodos != null) {
            nodos.get(ori).add_transicion(nodos.get(dest), auto);
        }
    }

    public nodo_estado build_nodo(boolean accept, boolean error, int tipo,
            String ID, int cad_l) {
        nodo_estado n = new nodo_estado();
        n.acept_st = accept;
        n.err_st = error;
        n.tipo_edo = tipo;
        n.id = ID;
        n.cad_l = cad_l;
        return n;
    }

    public nodo_estado build_nodo(boolean accept, boolean error, int tipo,
            String ID, int cad_l, boolean bucle) {
        nodo_estado n = build_nodo(accept, error, tipo,
                ID, cad_l);
        n.bucle = bucle;
        return n;
    }

    public boolean evaluar() {
        /*
        Evaluar tambien cuando un elemento es opcional, o no
        En una sola transicion, mandar a poner todas las cadenas aceptadas para la transicion
        En una sola transicion, mandar a poner todas los automatas aceptados para la transicion
        */
        // La transicion repetitiva es la que me falla
        ind=0;
        System.out.println(String.format("Evaluando el automata %s, el codigo es %s", this.ID, this.fuente));
        this.nodo_act = this.nodos.get(0);
        System.out.println(String.format("El indice inicial para el automata %s es %d", this.ID, this.ind));
        try {
            nodo_act.init_code = this.fuente.substring(ind); // Hay un error cuando se mete solo el +
            System.out.println(String.format("El codigo inicial para el nodo_act del automata %s es %s", this.ID, this.nodo_act.init_code));
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println(String.format("No hay código suficiente en el autómata %s", this.ID));
            return false;
        }
        
        System.out.println(String.format("Evaluando el nodo %s con el codigo %s",
                this.nodo_act.id, this.nodo_act.init_code));

        while (nodo_act.evaluar()) {
            this.ind += nodo_act.last_ind;
            System.out.println(String.format("El nodo %s esta correcto", this.nodo_act.id));
            /*
                Si tira error pero es un bucle, entonces regresar el indice a donde
                estaba y poner el nodo_act como get_next_nodo() y mandar a evaluar otra vez
                Para hacer esto, el automata que es un bucle, tendra qye haberse comprobado al menos una vez
                **Como meterlo otra vez al ciclo while de arriba??
            ***Ya quedo, se soluciono en la clase nodo_estado
             */
            
            
            if (this.nodo_act.get_next_nodo() != null) {
//                if (!this.nodo_act.get_next_nodo().bucle) {
//                    this.nodo_act = this.nodo_act.get_next_nodo();
//                }
                System.out.println("---");
                System.out.println("Indice en automata "+this.ID+": "+this.ind);
                
                if(this.nodo_act.rev>0){
                    if(this.nodo_act.aut != null)
                        this.nodo_act.aut.ind=0;
                }
                
                this.nodo_act = this.nodo_act.get_next_nodo();
                
                try {
                    if (this.fuente.substring(ind).length() < nodo_act.cad_l || this.nodo_act.cad_l < 0) {
                        /* Si el codigo restante es el menor a la cadena esperada
                            entonces se le envia lo que se tenga
                         */
                        nodo_act.init_code = this.fuente.substring(ind);
                    } else {
                        /*
                            En caso contrario, solo se envía la parte del código que se
                            va a utilizar.
                         */
                        nodo_act.init_code = this.fuente.substring(ind, (ind + nodo_act.cad_l));
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    /*
                    Si una de sus transiciones validas, contiene un automata
                    reptitivo y es true, entonces regreasr true desde aca
                    */
                    if(this.nodo_act.bucle || (this.nodo_act.transiciones == null && this.nodo_act.acept_st)){
                        String o = "Index out of bounds pero ";
                        o += (this.nodo_act.bucle) ? "es bucle" : ((this.nodo_act.transiciones == null && this.nodo_act.acept_st) ? "es terminal":"");
                        System.out.println(o);
                        System.out.println("El automata "+this.ID+" es correcto");
                        return true;
                    }
                    
                    System.out.println("Falta codigo en "+this.ID);
                    return false;
                }
            } else {
                /* Si este nodo es terminal, entonces
                verificar si sobra código y si es un estado de aceptación
                 */
                if (this.fuente.length() > this.ind) {
//                    if(this.nodo_act.bucle){
//                        continue;
//                    }
                    /*Quiere decir que sobra codigo*/
                    /*****Esto ya se controla en nodo_estado*****/
//                    System.out.println("Sobra codigo en "+this.ID);
//                    return false;
                }
                System.out.println("El automata "+this.ID+" es correcto");
                return true;
            }
            System.out.println(String.format("Evaluando el nodo %s con el codigo %s",
                this.nodo_act.id, this.nodo_act.init_code));
        }
        
        if(this.nodo_act.acept_st && this.nodo_act.rev>0 ){
            System.out.println("El ultimo nodo era un estado de aceptación. Pero sobra código");
            return true;
        }
        /*
        if(hola=='mundo') { hola = 'otro mundo;}
        */

        return false;
    }

}
