/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class sentencia_compuesta extends automata {
    
    public sentencia_compuesta(){}
    
    public sentencia_compuesta(String cod){super(cod);}
    
    public void build(){
        this.ID = "sentencia_compuesta";
        
        /*
        A
        |  {
        B <---------------
        |  sentencia     |
        C ---------------
        |  }
        D
        */
        
        this.nodos.add(this.build_nodo(false, false, 0, "sent_comp-A", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "sent_comp-B", -1));
        this.nodos.add(this.build_nodo(false, false, 1, "sent_comp-B", -1, true));
        this.nodos.add(this.build_nodo(true, false, 0, "sent_comp-C", 0));
        
        this.add_transicion(0, "{", 1);
        this.add_transicion(1, 2, new sentencia());
        this.add_transicion(2, 1, new sentencia());
        this.add_transicion(2, "}",3);
    }
    
}
