/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class operador_booleano extends automata{
    public operador_booleano(){}
    
    public operador_booleano(String cod){super(cod);}
    
    public void build(){
        this.ID = "operador_booleano";
        
        /*
        A
        | > | < | == | >= | <=
        B
        */
        
        this.nodos.add(this.build_nodo(false, false, 0, "oper_bool-A", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "oper_bool-B", 0));
        
        this.add_transicion(0, ">=",1);
        this.add_transicion(0, "<=",1);
        this.add_transicion(0, "==",1);
        this.add_transicion(0, ">",1);
        this.add_transicion(0, "<",1);
    }
}
