/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class expresion_booleanas extends automata{
    public expresion_booleanas(){}
    
    public expresion_booleanas(String cod){super(cod);}
    
    public void build(){
        this.ID = "expresion_booleana";
        /*
        A
        |  expresion
        B
        | operador_booleano
        C
        | expresion
        D
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "expr_bool-A", -1));
        this.nodos.add(this.build_nodo(false, false, 1, "expr_bool-B", -1));
        this.nodos.add(this.build_nodo(false, false, 1, "expr_bool-C", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "exp_bool-D", 0));
        
        this.add_transicion(0, 1, new expresion());
        this.add_transicion(1, 2, new operador_booleano());
        this.add_transicion(2, 3, new expresion());
        
    }
}
