/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class mayuscula extends automata{
    
    public mayuscula(String cod){
        super(cod);
    }
    
    public mayuscula(){
        super();
    }
    
    
    public void build(){
        this.ID = "mayuscula";
        
        this.nodos.add(this.build_nodo(false, false, 0, "mayuscula-A", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "mayuscula-B", 0));
        
        String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(String l : letras.split("")){
            this.add_transicion(0, l, 1);
        }
    }
    
}
