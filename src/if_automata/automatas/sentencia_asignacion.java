/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class sentencia_asignacion extends automata {
    public sentencia_asignacion(){}
    
    public sentencia_asignacion(String cod){super(cod);}
    
    public void build(){
        this.ID = "sent_asignac";
        
        /*
        A
        | variable
        B
        | =
        C 
        | expresion
        D
        | ;
        E
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "sent_asignac-A", -1));
        this.nodos.add(this.build_nodo(false, false, 0, "sent_asignac-B", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "sent_asignac-C", -1));
        this.nodos.add(this.build_nodo(false, false, 0, "sent_asignac-D", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "sent_asignac-E", 0));
        
        this.add_transicion(0, 1, new variable());
        this.add_transicion(1, "=", 2);
        this.add_transicion(2, 3, new expresion());
        this.add_transicion(3, ";", 4);
    }
}
