/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author fredy
 */
public class signo extends automata {
    
    public signo(String cod) {
        super(cod);
    }
    
    public signo(){
        
    }
    
    public void build(){
        
        /*
        A
        | + | -
        B
        */
        
        this.ID = "signo";
        this.nodos.add(this.build_nodo(false, false, 0, "signo-A", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "signo-B", 0));
        this.add_transicion(0, "+", 1);
        this.add_transicion(0, "-", 1);
    }
    
}
