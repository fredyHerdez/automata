/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class parametros extends automata{
    public parametros(){}
    
    public parametros(String cod){super(cod);}
    
    public void build(){
        this.ID = "parametros";
        /*
        A
        |  expresion
        B
        |  ,
        A
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "parametros-A", -1, true));
        this.nodos.add(this.build_nodo(true, false, 0, "parametros-B", 1, true));
//        this.nodos.add(this.build_nodo(false, false, 1, "parametros-C", -1, true));
//        this.nodos.add(this.build_nodo(true, false, 0, "paramteros-D", 1, true));
        
        this.add_transicion(0, 1, new expresion());
        this.add_transicion(1, ",", 0);
//        this.add_transicion(2, 3, new expresion());
//        this.add_transicion(3, ",", 2);
    }
}
