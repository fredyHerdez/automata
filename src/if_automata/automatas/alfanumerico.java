/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class alfanumerico extends automata {
    
    public alfanumerico(){
        
    }
    
    public alfanumerico(String cod){
        super(cod);
    }
    
    public void build(){
        this.ID = "alfanumerico";
        this.nodos.add(this.build_nodo(false, false, 1, "alfan-A", 1));
        this.nodos.add(this.build_nodo(true, false, 1, "alfan-B", 1));
        
        this.add_transicion(0, 1, new mayuscula());
        this.add_transicion(0, "_", 1);
        this.add_transicion(0, 1, new minuscula());
        this.add_transicion(0, 1, new digito());
        
    }
    
}
