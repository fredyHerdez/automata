/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class letra extends automata {
    public letra(){}
    
    public letra(String cod){super(cod);}
    
    public void build(){
        this.ID = "letra";
        
        /*
        A
        | may | min | _
        B
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "letra-A", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "letra-B", 0));
        
        this.add_transicion(0, 1, new mayuscula());
        this.add_transicion(0, 1, new minuscula());
        this.add_transicion(0, "_", 1);
    }
}
