/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author fredy
 */
public class block_if extends automata{
    
    public block_if(){
        
    }
    
    public block_if(String cod){
        super(cod);
    }
    
    public void build(){
        this.ID = "Bloque if";
        this.nodos.add(this.build_nodo(false, false, 0, "A", 2));
        this.nodos.add(this.build_nodo(false, false, 0, "B", 9));
        this.nodos.add(this.build_nodo(false, false, 0, "C", 4));
        this.nodos.add(this.build_nodo(false, false, 0, "D", 9));
        this.nodos.add(this.build_nodo(true, false, 0, "E", 0));
        
        
        this.add_transicion(0, "if", 1);
        this.add_transicion(1, "condicion", 2);
        this.add_transicion(2, "then", 3);
        this.add_transicion(3, "expresion", 4);
    }
    
    
}
