/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author fredy
 */
public class entero extends automata {
    
    public entero(String cod) {
        super(cod);
    }
    
    public entero(){
        
    }
    
    public void build(){
        this.ID = "entero";
        /*
        A
        |  + | -
        B
        |  { digito }
        C
        */
        this.nodos.add(this.build_nodo(false, false, 1, "entero-A", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "entero-B", 1));
        this.nodos.add(this.build_nodo(true, false, 1, "entero-C", 1, true));
        
        this.add_transicion(0, 1, new signo()); //Checar si se le pasa el codigo a tiempo
        this.add_transicion(1, 2, new digito());
        this.add_transicion(2, 2, new digito());
        
    }
    
}
