/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class literal_boolean extends automata{
    public literal_boolean(){}
    
    public literal_boolean(String cod){super(cod);}
    
    public void build(){
        /*
        A
        |  true | false
        B
        */
        this.ID = "liteal_boolean";
        
        this.nodos.add(this.build_nodo(false, false, 0, "lit_boolean-A", 5));
        this.nodos.add(this.build_nodo(true, false, 0, "lit_boolean-B", 1));
        
        this.add_transicion(0, "true", 1);
        this.add_transicion(0, "false", 1);
        
    }
}
