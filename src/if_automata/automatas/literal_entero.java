/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class literal_entero extends automata {
    
    public literal_entero(){}
    
    public literal_entero(String cod){super(cod);}
    
    public void build(){
        this.ID = "literal_entero";
        
        /*
        A
        | signo
        B
        | { digito }
        C
        */
        
        this.nodos.add(this.build_nodo(false, false, 0, "entero-A", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "entero-B", 1));
        this.nodos.add(this.build_nodo(true, false, 1, "entero-C", 1, true));
        
        this.add_transicion(0, "+", 1); //Checar si se le pasa el codigo a tiempo
        this.add_transicion(0, "-", 1);
        this.add_transicion(0, 2, new digito());
        this.add_transicion(1, 2, new digito());
        this.add_transicion(2, 2, new digito());
    }
    
}
