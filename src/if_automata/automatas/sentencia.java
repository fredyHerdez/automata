/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class sentencia extends automata {
    public sentencia(){}
    
    public sentencia(String cod){super(cod);}
    
    public void build(){
        this.ID = "sentencia";
        
        /*
        A
        |   llamada_funcion | sentencia_asignacion | estructura de control
        B
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "sentencia-A", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "sentencia-B", 0));
        
        this.add_transicion(0, 1, new llamada_funcion());
        this.add_transicion(0, 1, new sentencia_asignacion());
//        this.add_transicion(0, 1, new alternativa()); //Causa stack over flow si lo quiero anidar :$
    }
}
