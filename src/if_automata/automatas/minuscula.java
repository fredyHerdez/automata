/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class minuscula extends automata {
    public minuscula(String cod){
        super(cod);
    }
    
    public minuscula(){
        super();
    }
    
    
    public void build(){
        this.ID = "minuscula";
        
        this.nodos.add(this.build_nodo(false, false, 0, "minuscula-A", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "minuscula-B", 0));
        
        String letras = "abcdefghijklmnopqrstuvwxyz";
        for(String l : letras.split("")){
            this.add_transicion(0, l, 1);
        }
    }
}
