/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class expresion extends automata{
    public expresion(){}
    
    public expresion(String cod){super(cod);}
    
    public void build(){
        this.ID = "expresion";
        
        /*
        A
        | literal | identificador | variable | llamada_funcion
        B
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "expresion-A", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "expresion-B", 0));
        
        this.add_transicion(0, 1, new literal());
        this.add_transicion(0, 1, new identificador());
        this.add_transicion(0, 1, new variable());
    }
}
