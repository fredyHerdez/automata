/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class caracter extends automata{
    public caracter(){
        
    }
    
    public caracter(String cod){
        super(cod);
    }
    
    public void build(){
        this.ID = "caracter";
        this.nodos.add(this.build_nodo(false, false, 1, "caracter-A", 1));
        this.nodos.add(this.build_nodo(true, false, 1, "caracter-B", 1));
        
        this.add_transicion(0, "-", 1);
        this.add_transicion(0, 1, new alfanumerico());
        this.add_transicion(0, ",", 1);
//        this.add_transicion(0, ";", 1);
        this.add_transicion(0, "$", 1);
    }
    
}
