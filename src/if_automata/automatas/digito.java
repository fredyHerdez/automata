/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

import if_automata.nodo_estado;
import java.util.List;

/**
 *
 * @author fredy
 */
public class digito extends automata {
    
    
    public digito(String cod){
        super(cod);
    }
    
    public digito(){
        
    }
    
    public void build(){
        this.ID = "digito";
        this.nodos.add(this.build_nodo(false, false, 0, "digito-A", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "digito-B", 0));
        for (int i = 0; i < 10; i++) {
            this.add_transicion(0, Integer.toString(i), 1);
        }
    }
    
}
