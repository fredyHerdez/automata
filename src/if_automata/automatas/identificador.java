/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class identificador extends automata {
    public identificador(){}
    
    public identificador(String cod){super(cod);}
    
    public void build(){
        this.ID = "identificador";
        
        /*
        A
        | letra
        B
        | { letra | digito } *
        C
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "identificador-A", 1));
        this.nodos.add(this.build_nodo(true, false, 1, "identificador-B", -1, true));
//        this.nodos.add(this.build_nodo(true, false, 1, "identificador-C", -1, true));
        
        this.add_transicion(0, 1, new letra());
        this.add_transicion(1, 1, new letra());
        this.add_transicion(1, 1, new digito());
//        this.add_transicion(2, 2, new letra());
//        this.add_transicion(2, 2, new digito());
        
    }
}
