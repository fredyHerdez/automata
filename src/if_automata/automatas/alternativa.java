/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class alternativa extends automata{
    
    public alternativa(){}
    
    public alternativa(String cod){super(cod);}
    
    public void build(){
        this.ID = "alernativa";
        
        /*
        A
        |  if
        B
        |  (
        C
        |  condicion
        D
        |  )
        E
        |  sentencia_compuesta
        F
        |  else                       if
        G---------------------------------------B
        |  sentencia_compuesta
        H
        */
        
        this.nodos.add(this.build_nodo(false, false, 0, "alternativa-A", 2));
        this.nodos.add(this.build_nodo(false, false, 0, "alternativa-B", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "alternativa-C", -1));
        this.nodos.add(this.build_nodo(false, false, 0, "alternativa-D", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "alternativa-E", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "alternativa-F", 4));
        this.nodos.add(this.build_nodo(false, false, 1, "alternativa-G", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "alternativa-H", 0));
        
        this.add_transicion(0, "if", 1);
        this.add_transicion(1, "(", 2);
        this.add_transicion(2, 3, new condicion());
        this.add_transicion(3, ")", 4);
        this.add_transicion(4, 5, new sentencia_compuesta());
        this.add_transicion(5, "else", 6);
        this.add_transicion(6, 7, new sentencia_compuesta());
        
    }
    
    
    
}
