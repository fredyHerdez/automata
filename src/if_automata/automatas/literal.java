/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class literal extends automata{
    
    public literal(){}
    
    public literal(String cod){super(cod);}
    
    public void build(){
        this.ID = "literal";
        
        /*
        A
        | lit_string | lit_entero | lit_boolean
        B
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "literal-A", 1));
        this.nodos.add(this.build_nodo(true, false, 1, "literal-B", 0));
        
        this.add_transicion(0, 1, new literal_entero());
        this.add_transicion(0, 1, new literal_boolean());
        this.add_transicion(0, 1, new literal_string());
    }
    
}
