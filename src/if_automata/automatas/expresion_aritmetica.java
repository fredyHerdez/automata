/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class expresion_aritmetica extends automata{
    
    public expresion_aritmetica(){}
    
    public expresion_aritmetica(String cod){super(cod);}
    
    public void build(){
        this.ID = "expresion_aritmetica";
        /*   5 + 8
        A
        | literal_entero
        B  
        |  operador_aritmetico
        A
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "exp_arit-A", -1, true));
        this.nodos.add(this.build_nodo(true, false, 1, "exp_arit-B", -1, true));
        
        this.add_transicion(0, 1, new literal_entero());
        this.add_transicion(0, 1, new identificador());
        this.add_transicion(1, 0, new operador_aritmetico());
    }
    
}
