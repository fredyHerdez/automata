/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class condicion extends automata{
    
    public condicion(){}
    
    public condicion(String cod){super(cod);}
    
    public void build(){
        this.ID = "condicion";
        
        /*
        A
        |  expresion_booleanas
        B
        |  operador_logico
        A
        */
        
        this.nodos.add(this.build_nodo(false, false, 1, "condicion-A", -1));
        this.nodos.add(this.build_nodo(true, false, 1, "condicion-B",-1, true));
        
        this.add_transicion(0, 1, new expresion_booleanas());
        this.add_transicion(1, 0, new operador_logico());
    }
    
}
