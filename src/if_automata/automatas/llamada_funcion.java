/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class llamada_funcion extends automata {
    
    public llamada_funcion(){}
    
    public llamada_funcion(String cod){super(cod);}
    
    
    public void build(){
        this.ID = "llamada_funcion";
        
        /*
        A
        |  identificador
        B
        |  (
        C
        |  parametros ?
        D
        |  )
        E
        |  ;
        F
        */
        this.nodos.add(this.build_nodo(false, false, 1, "llam_func-A", -1));
        this.nodos.add(this.build_nodo(false, false, 0, "llam_func-B", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "llam_func-C", -1));
        this.nodos.add(this.build_nodo(false, false, 0, "llam_func-D", 1));
        this.nodos.add(this.build_nodo(false, false, 0, "llam_func-B", 1));
        this.nodos.add(this.build_nodo(true, false, 0, "llam_func-B", 0));
        
        this.add_transicion(0, 1, new identificador());
        this.add_transicion(1, "(", 2);
        this.add_transicion(2, ")", 4);
        this.add_transicion(2, 3, new parametros());
        this.add_transicion(3, ")", 4);
        this.add_transicion(4, ";", 5);
    }
    
}
