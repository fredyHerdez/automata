/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class literal_string extends automata {
    
    public literal_string(){
        
    }
    
    public literal_string(String cod){
        super(cod);
    }
    
    
    public void build(){
        this.ID = "lit_string";
        
        /*
        A
        |  '
        B
        |  {caracter}
        B
        |  '
        D
        */
        
        this.nodos.add(this.build_nodo(false, false, 0, "lit_string-A", 1));
        this.nodos.add(this.build_nodo(false, false, 1, "lit_string-B", 1, true));
        this.nodos.add(this.build_nodo(true, false, 0, "lit_string-C", 1));
//        this.nodos.add(this.build_nodo(true, false, 0, "lit_string-D", 0));
        
        
        this.add_transicion(0, "'", 1);
        this.add_transicion(1, 1, new caracter());
//        this.add_transicion(2, 2, new caracter());
        this.add_transicion(1, "'", 2);
        
    }
    
}
