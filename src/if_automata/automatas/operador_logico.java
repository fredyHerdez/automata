/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata.automatas;

/**
 *
 * @author ASUS
 */
public class operador_logico extends automata {
    public operador_logico(){}
    public operador_logico(String cod){super(cod);}
    
    public void build(){
        this.ID = "operador_logico";
        
        /*
        A
        |  &  | ||
        B
        */
        
        this.nodos.add(this.build_nodo(false, false, 0, "oper_logi-A", -1));
        this.nodos.add(this.build_nodo(true, false, 0, "oper_logi-B", 0));
        
        this.add_transicion(0, "||", 1);
        this.add_transicion(0, "&", 1);
    }
    
}
