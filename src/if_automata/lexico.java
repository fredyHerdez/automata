/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author root
 */
public class lexico {
    
    // Leer el codigo fuente y analizar lexicamente
    
    String cod_fuente;
    
    public void set_fuente(String fuente){
        this.cod_fuente = fuente;
    }
    
    
    public lexico(String codigo){
        this.cod_fuente = codigo;
    }
    
    
    String alfabeto = "'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-*/-_$,{}()&|!><=;.\t";
    
    
    public String comprobar(){
        List<String> list_carac = Arrays.asList(this.cod_fuente.split(""));
        for(String s : list_carac){
            if(!s.equals(" ") && !s.equals("\n")){
                if(!test_char(s))
                    return s;
            }
        }
        
        return null;
    }
    
    public boolean test_char(String s){
        for(String c : Arrays.asList(this.alfabeto.split(""))){
                if(s.equals(c))
                    return true;
            }
        return false;
    }
    
}
