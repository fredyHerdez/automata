/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if_automata;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author root
 */
public class Fichero {
    
    //Leer, guardar y eliminar fichero codigo fuente, con ventanas y todo
    
    JFileChooser chooser;
    FileNameExtensionFilter fnef;
    String file_name;
    File fichero;
    
        
    public String open_file_box(Component c){
        chooser = new JFileChooser();
        fnef = new FileNameExtensionFilter("TEXT FILES",
                "txt", "fy");
        chooser.setFileFilter(fnef);
        chooser.setCurrentDirectory(new File("/home/fredy")); //*****
        int resp = chooser.showOpenDialog(c);
        if(resp == JFileChooser.APPROVE_OPTION){
            try {
                String fte = readFile(chooser.getSelectedFile());
                chooser=null;
                fnef=null;
                return fte;
            } catch (IOException ex) {
                // Logger.getLogger(Fichero.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(c, "Ha ocurrido un error mientras se abría el archivo.");
            }
        }
        return null;
    }
    
    
    public String readFile(File f) throws FileNotFoundException, IOException{
        fichero = f;
        BufferedReader br = new BufferedReader(new FileReader(fichero));
        String texto = "";
        String ln;
        while((ln=br.readLine()) != null){
            texto += ln + "\n";
        }
        br.close();
        f=null;
        return texto;
    }
    
    public boolean save(String codigo, Component c){
        
        if(fichero!=null){
            try {
                save_file(fichero, codigo);
                return true;
            } catch (IOException ex) {
//                Logger.getLogger(Fichero.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(c, "Ha ocurrido un error mientras se guardaba el archivo.");
            }
        }else{
            return save_file_box(codigo, c);
        }
        return false;
    }
    
    public boolean save_file_box(String codigo, Component c){
        chooser = new JFileChooser();
        fnef = new FileNameExtensionFilter("TEXT FILES",
                "txt", "fy");
        chooser.setFileFilter(fnef);
        chooser.setCurrentDirectory(new File("/home/fredy"));
        int resp = chooser.showSaveDialog(c);
        if(resp == JFileChooser.APPROVE_OPTION){
            try {
                save_file(chooser.getSelectedFile(), codigo);
                chooser = null;
                fnef = null;
                return true;
            } catch (IOException ex) {
                // Logger.getLogger(Fichero.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(c, "Ha ocurrido un error mientras se guardaba el archivo.");
            }
        }
        return false;
    }
    
    public void save_file(File f, String cod) throws IOException{
        FileWriter fw = new FileWriter(f);
        fw.write(cod);
        fw.close();
    }
    
    
}
